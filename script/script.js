
let body = document.querySelector('body');
let btn = document.querySelector('.change');
let lastColor = localStorage.getItem('lastColor')

if (lastColor != ""){
    body.style.setProperty('--color-title', lastColor)
}

btn.addEventListener('click', () => {
    localStorage.setItem('--color-title','yellow');
    let colorBody = body.style.getPropertyValue('--color-title');
    let colorStorage = localStorage.getItem('--color-title')

    if( colorBody == colorStorage){
        body.style.setProperty('--color-title', 'white');
    } else {
    body.style.setProperty('--color-title', colorStorage) ;
    }

    let colorNow = body.style.getPropertyValue('--color-title');
    localStorage.setItem('lastColor', colorNow)
})